// First part ---------------
let inputName = prompt("Enter your  name:");
let finalReverse = inputName.split("");
finalReverse = finalReverse.reverse();
finalReverse = finalReverse.join("");

function userName() {
    if(finalReverse.length <= 7) {
        for(i = 0; i < finalReverse.length; i++) {
            console.log(finalReverse[i]);
        };
    }

    else{
        for(i = 0; i < finalReverse.length; i++) {
            if(finalReverse[i] == "a" || finalReverse[i] == "e" || finalReverse[i] == "i" || finalReverse[i] == "o" || finalReverse[i] == "u"){
                continue;
            }
            else{
                console.log(finalReverse[i]);
            }
        }
    }
};
userName();

// Second part -------------------
let students = ["John", "Jane", "Joe"];
console.log(students);

// 1. Total size of an array
console.log(students.length);

// 2. Adding a new element at the beginning of an array
students.unshift("Jack");

// 3. Adding a new element at the end of an array
students.push("Jill");

// 4. Removing an existing element at the beginning of an array
students.shift();

// 5. Removing an existing element at the end of an array
students.pop();

// 6. Find the index number of the last element in an array
console.log(students.length - 1);

// 7. Print each element of the array in the browser console.
for(i = 0; i < students.length; i++){
    console.log(students[i]);
};
// OR -----
console.log(students);


// 8. Using students array, create a function that will look for the index of a given students and will remove the name of the students from the array list
function removeIndex(index) {
        students.splice(index, 1);
        console.log(students);
}
removeIndex(1);


// Third part ----------------
let person = {
    firstName: "Juan",
    lastName: "Dela Cruz",
    nickname: "J",
    age: "25",
    address: {
        city: "Quezon City",
        country: "Philippines"
    },
    friends: {
        firstFriend: "John",
        secondFriend: "Jane",
        thirdFriend: "Joe"
    },

    output(firstName, lastName, nickname, age, city, country, firstFriend, secondFriend, thirdFriend){
        console.log("My name is " + person.firstName + " " + person.lastName + ", but you can call me " + person.nickname + ". I am " + person.age + " years old and I live in " + this.address.city + ", " + this.address.country + ". My friends are " + this.friends.firstFriend + ", " + this.friends.secondFriend + ", and " + this.friends.thirdFriend + ".");
    }
};
person.output();

// 4th part ---------

let newPerson = {...person}; 
console.log(newPerson);


newPerson.output = function newOutput(firstName, lastName, nickname, age, city, country, firstFriend, secondFriend, thirdFriend) {
    console.log(`My name is ${newPerson.firstName} ${newPerson.lastName}, but you can call me ${newPerson.nickname}. I am ${newPerson.age} years old and I live in ${this.address.city}, ${this.address.country}. My friends are ${this.friends.firstFriend}, ${this.friends.secondFriend}, and ${this.friends.thirdFriend}.`)

};
newPerson.output();